# README #

## Debugging ##
### Installing NVM on Mac OS X ###

```
curl https://raw.githubusercontent.com/creationix/nvm/v0.13.1/install.sh | bash
```
Add the following lines to `~/.bash_profile` (edit for own user directory name):
```
export NVM_DIR="/Users/rrova/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
```
Then: 
```
chmod +x ~/.bash_profile
~/.bash_profile
env # check environment variables
nvm install v0.12.0
```
Then install node-inspector (may need sudo): 
```
npm install -g node-inspector
```
and run it in a terminal window:
```
node-inspector
```
Visit `http://127.0.0.1:8080/debug?port=5858` to start debugging.

Run the meteor program to be debugged by opening a terminal window in the program directory and invoke:
```
env NODE_OPTIONS="--debug" meteor
```
or to break on the first line of code:
```
env NODE_OPTIONS="--debug-brk" meteor
```
Products = new Meteor.Collection('products');

TabularTables = {};

TabularTables.Products = new Tabular.Table({
    headerCallback: function( thead, data, start, end, display ) {
      $(thead).find('th').eq(0).css('display', 'none');
    },
    createdRow: function( row, data, dataIndex ) {
      $(row).children('td').eq(0).css('display', 'none');
    },
    name: "ProductList",
    collection: Products,
    extraFields: ['short_url', 'name'],
    columns: [
        {
          data: 'name', title: "hidden name"
        },
        {
            data: 'name_url()', title: "Name"
        },
        {data: "price", title: "Price", render: function(val){
            return numeral(val/100.0).format('$0,0.00');
        }},
        {data: "view_count", title: "View Count"},
        {data: "sales_count", title: "Sales Count"},
        {data: "sales_usd_cents", title: "Total Sales", render: function(val){
            return numeral(val/100.0).format('$0,0.00');
        }},
        {data: "description", title: "Description"}
    ]
});

// TODO only allow on server for security
Products.allow({
    insert: function (document) {
        return true;
    },
    update: function () {
        return true;
    },
    remove: function () {
        return true;
    }
});

Products.helpers({
    name_url: function () {
        return '<a href="' + this.short_url + '" target="_blank">' + this.name + '</a>';
    }
});

if (Meteor.isClient) {
    Meteor.startup(function () {
        AccountsEntry.config({
            language: 'en',
            homeRoute: '/',
            dashboardRoute: '/admin/products/'
        });
        Accounts.ui.config({
            extraSignupFields: [],
            requestPermissions: {
               gumroad: ['view_sales', 'edit_products']
            }
        });
     });

    Template.registerHelper('TabularTables', TabularTables);

    Template.productAdmin.created = function() {
        console.log("productAdmin template created");
        if (!this.created){
            Session.set('productFetchInProgress', true);
            console.log("calling getProducts");
            Meteor.call('getProducts', function(error, id) {
                Session.set('productFetchInProgress', false);
            });
            this.created = true;
        }
    };

    Template.productAdmin.rendered = function () {
        console.log("productAdmin template rendered");
    };

    Template.about.created = function() {
        console.log("about template created");
    };

    Template.about.rendered = function () {
        console.log("about template rendered");
    };

    Template.layout.created = function() {
        console.log("layout template created");
    };

    Template.layout.rendered = function () {
        console.log("layout template rendered");
    };

    Template.adminDropdown.rendered = function() {
        // init bootstrap functionality:
        this.$('[data-toggle="dropdown"]').dropdown();
    }

    Meteor.subscribe('products');

    // fix for accounts-entry login error (chokes on 'undefined'):
    Accounts.ui._options.requestOfflineToken = {};

    Template.products.helpers({
        productFetchInProgress: function() {
          return Session.get('productFetchInProgress');
        }
     });
    Template.products.events({
        'click button.getProducts': function(e, t) {
          e.preventDefault();
          Session.set('productFetchInProgress', true);
          Meteor.call('getProducts', function(error, id) {
            Session.set('productFetchInProgress', false);
          });
        }
      });
      // Helper that adds 'active' class to a nav link depending on the current page:
         Template.navBar.helpers({
           activeRouteClass: function(/* route names */) {
             var args = Array.prototype.slice.call(arguments, 0);
             args.pop();

             var active = _.any(args, function(name) {
               return Router.current() && Router.current().route.name === name
             });

             return active && 'active';
           }
         });
}

if (Meteor.isServer) {
    Meteor.publish('products', function() {
        return Products.find({}, {sort: {name: 1}});
      });

      Meteor.startup(function () {
          ServiceConfiguration.configurations.remove({
            service: 'gumroad'
          });
          ServiceConfiguration.configurations.insert({
            service: 'gumroad',
            clientId: '3cb0f0a9cddc2f2c34b96dacb20d62ac7f9d46697b2b05996ba3cd6147821dea',
            secret: '9e8df97be38d98ac914a9290c04fa988d55c4facdd68fcff11016d4c29f25c0b'
          });

        Meteor.methods({
         getProducts: function() {
             var accessToken = Meteor.user().services.gumroad.accessToken;
             var params = { access_token: accessToken };
             var options = { params: params };
             var response = HTTP.get('https://api.gumroad.com/v2/products', options);
             if (response.data.success) {
                   response.data.products.forEach(function(item) {
                       // Upsert instead of insert so that
                       // we don't duplicate existing items from
                       // previous API calls:
                       Products.upsert({id: item.id}, item);
                   });
             }
             else {
                   // TODO add error
             }
         } // end getProducts()
      }); // end Meteor.methods()
  }); // end startup()
} // end isServer

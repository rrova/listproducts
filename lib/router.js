Router.configure({
  layoutTemplate: 'layout',
  waitOn: function() { return Meteor.subscribe('products'); }
});

Router.map(function() {
    this.route('home', {path: '/'});
    //this.route('login', {path: '/sign-in'});
    this.route('productAdmin', {path: '/admin/products'});
    this.route('about', {path: '/about'});
    // Manually create a CSV for download:
    // TODO fix security
    // This route is currently accessible even when user is not logged in,
    // and cannot be fixed by simply adding to Router.onBeforeAction() at
    // the bottom of this file, because on the server Meteor.user()
    // is not a valid call. Will require more robust solution, but works for proof of concept.
    this.route('download', {
         path: 'download/products',
         where: 'server',
         action: function() { 
              // manually create a CSV for download:
              var products = Products.find().fetch();
              var csv = 'Name,Price,Description\r\n'; // CSV column titles
              var now = moment().format('YYYY-MM-DD-HH:mm');
              var filename = 'gumroadProducts-' + now + '.csv';
              var contentDisposition = 'attachment;filename=' + filename;

              for(var i=0; i<products.length; i++) {
                  // Escape any double quotes in the name:
                  var name = '';
                  if(products[i].name) {
                      name = products[i].name.replace(/"/g, '""');
                  }
                  // Escape any double quotes in the description:
                  var description = '';
                  if(products[i].description) {
                      description = products[i].description.replace(/"/g, '""');
                      // remove HTML tags
                      description = description.replace(/(<([^>]+)>)/ig,"");
                  } 
                  // Append this product's info to the CSV string:
                  csv += '"' + name + '","' + products[i].price + '","' + description + '"' + '\r\n'
              }
              // Send the response, with appropriate headers so the browser treats it as a download:
              this.response.writeHead(200, {
                  'Content-Type': 'text/csv',
                  'Content-Disposition': contentDisposition
              });
              this.response.end(csv);
          } // end createProductsCSV()
     }); // end doDownload server-side route
}); // end Route.map()

var requireLogin = function() {
  if (! Meteor.user()) {
      Router.go(Router.path('entrySignIn'));
  }
  else {
      this.next();
  }
}

// Restrict these routes to logged-in users:
Router.onBeforeAction(requireLogin, {only: ['productAdmin']});
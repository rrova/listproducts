Template.configureLoginServiceDialogForGumroad.siteUrl = function () {
  return Meteor.absoluteUrl();
};

Template.configureLoginServiceDialogForGumroad.fields = function () {
  return [
    {property: 'clientId', label: 'API Key'},
    {property: 'secret', label: 'Secret Key'}
  ];
};

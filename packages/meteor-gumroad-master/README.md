meteor-gumroad
===============
Forked from https://github.com/yefim/meteor-gumroad.git

A OAuth2 wrapper for the gumroad API

##Installation

* Install accounts-ui pacakge: `meteor add accounts-ui`
* `meteor add gumroad`

##Usage

Creates a new function `Meteor.loginWithGumroad(options, callback)`

This is the backbone of `accounts-gumroad`

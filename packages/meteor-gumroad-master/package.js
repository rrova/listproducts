Package.describe({
  summary: "A OAuth2 wrapper for the Gumroad API",
  version: "1.0.0",
    git: "",
    name: "meteor-gumroad-master"
});

Package.onUse(function(api) {

  api.use('oauth@1.0.1', ['client', 'server']);
  api.use('oauth2@1.0.0', ['client', 'server']);
  api.use('http@1.0.4', ['client', 'server']);
  api.use('templating@1.0.5', 'client');
  api.use('service-configuration@1.0.1', ['client', 'server']);

  api.export('gumroad');

  api.addFiles(
    ['gumroad_configure.html', 'gumroad_configure.js'],
    'client');
  api.addFiles('gumroad_common.js', ['client', 'server']);
  api.addFiles('gumroad_server.js', 'server');
  api.addFiles('gumroad_client.js', 'client');
});


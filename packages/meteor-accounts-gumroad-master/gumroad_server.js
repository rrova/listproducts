// v0.6.5
// Accounts.oauth.registerService('gumroad');

Accounts.addAutopublishFields({
  // publish all fields including access token, which can legitimately
  // be used from the client (if transmitted over ssl or on
  // localhost). https://developer.gumroad.com/documents/authentication
  forLoggedInUser: ['services.gumroad']
  // forOtherUsers: [
  //   'services.gumroad.id', 'services.gumroad.firstName', 'services.gumroad.lastName'
  // ]
});

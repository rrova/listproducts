//Accounts.oauth.registerService('gumroad');
// Meteor.loginWithGumroad = function (options, callback) {
//   var credentialRequestCompleteCallback = Accounts.oauth.credentialRequestCompleteHandler(callback);
//   GumRoad.requestCredential(options, credentialRequestCompleteCallback);
// };

  Meteor.loginWithGumRoad = function(options, callback) {
    // support a callback without options
    if (! callback && typeof options === "function") {
      callback = options;
      options = null;
    }
    var credentialRequestCompleteCallback = Accounts.oauth.credentialRequestCompleteHandler(callback);
    gumroad.requestCredential(options, credentialRequestCompleteCallback);
  };

  // Make it work with 0.9.3
  Meteor.loginWithGumroad = Meteor.loginWithGumRoad;

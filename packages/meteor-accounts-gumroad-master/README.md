meteor-accounts-gumroad
============================

A meteor package for GumRoad's login service.

Package Dependencies
----------------------

* accounts-base
* accounts-oauth
* gumroad

Install
-----------
```
meteor add meteor-accounts-gumroad
```


Package.describe({
  summary: "Accounts service for GumRoad accounts",
  version: "1.0.0",
    git: "",
    name: "meteor-accounts-gumroad-master"
});

Package.onUse(function(api) {

  api.use('accounts-base@1.0.1', ['client', 'server']);
  api.use('accounts-oauth@1.0.0', ['client', 'server']);
  api.use('meteor-gumroad-master@1.0.0', ['client', 'server']);

  api.addFiles(['gumroad_login_button.css'], 'client');
  api.addFiles('gumroad_common.js', ['client', 'server']);
  api.addFiles('gumroad_server.js', 'server');
  api.addFiles('gumroad_client.js', 'client');
});



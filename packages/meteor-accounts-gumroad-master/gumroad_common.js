// v0.6.5
Accounts.oauth.registerService('gumroad');

if (!Accounts.gumroad) {
  Accounts.gumroad = {};
}
